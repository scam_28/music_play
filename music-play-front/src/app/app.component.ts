import { Component, OnInit } from '@angular/core';
import { Music } from './models/music';
import { DeezerService } from './services/deezer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  displaySideBar: boolean = false;
  songs: Array<Music> = [];
  favourites: Array<Music> = [];
  query: string = '';
  constructor(private deezerService: DeezerService) {}
  ngOnInit() {
    this.initSongs();
  }

  initSongs() {
    this.deezerService.getAllSongs().subscribe((res) => {
      let response: any = res;
      this.songs = response.data;
    });
  }

  search() {
    this.deezerService.searchSong(this.query).subscribe((res) => {
      let response: any = res;
      this.songs = response.data;
    });
  }

  addFavourite(songFav: any) {
    this.favourites.push(songFav);
  }
}
