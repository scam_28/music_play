export class Album{
    id!:number;
    title!:string;
    cover!:string;
    cover_small!:string;
    cover_medium!:string;
    cover_big!:string;
}