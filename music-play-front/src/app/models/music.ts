import { Album } from './album';
import { Artist } from './artist';

export class Music {
  id!: number;
  title!: string;
  title_short!: string;
  title_version!: string;
  link!: string;
  duration!: number;
  rank!: number;
  preview!: string;
  md5_image!: string;
  album!: Album;
  artist!: Artist;
  position!: number;
}
