import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DeezerService {
  constructor(private http: HttpClient) {}

  getAllSongs(){
    let url = "api.deezer.com/chart/0/tracks";
    return this.http.get(url);
  }

  searchSong(query:string){
    let url = `api.deezer.com/search?q=${query}`;
    return this.http.get(url);
  }
}
