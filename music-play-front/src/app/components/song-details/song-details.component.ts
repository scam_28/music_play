import { Component, Input, OnInit } from '@angular/core';
import { Music } from 'src/app/models/music';

@Component({
  selector: 'app-song-details',
  templateUrl: './song-details.component.html',
  styleUrls: ['./song-details.component.css'],
})
export class SongDetailsComponent implements OnInit {
  @Input('song') song!: Music;
  constructor() {}

  ngOnInit(): void {}

  time() {
    let min = Math.round(this.song.duration / 60);
    let segs = this.song.duration - min * 60;
    return `${min}:${segs}`;
  }
}
