import { Component, Input, OnInit } from '@angular/core';
import { Music } from 'src/app/models/music';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css'],
})
export class FavouritesComponent implements OnInit {
  @Input('favourites') songs: Array<Music> = [];
  constructor() {}

  ngOnInit(): void {}
}
