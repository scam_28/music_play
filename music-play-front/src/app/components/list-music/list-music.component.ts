import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Music } from 'src/app/models/music';

@Component({
  selector: 'app-list-music',
  templateUrl: './list-music.component.html',
  styleUrls: ['./list-music.component.css'],
})
export class ListMusicComponent implements OnInit {
  constructor() {}
  @Input('songs') songs: Array<Music> = [];
  @Output() addFavourite = new EventEmitter();

  songDetail: Music = new Music();

  ngOnInit() {}

  getDetails(song: Music) {
    this.songDetail = song;
  }

  addToFavourites(song: Music) {
    this.addFavourite.emit(song);
  }
}
