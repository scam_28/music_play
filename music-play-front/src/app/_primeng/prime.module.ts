import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { SidebarModule } from 'primeng/sidebar';
import { CardModule } from 'primeng/card';
import {OverlayPanelModule} from 'primeng/overlaypanel';

@NgModule({
  exports: [ButtonModule, SidebarModule, CardModule, OverlayPanelModule],
  imports: [ButtonModule, SidebarModule, CardModule, OverlayPanelModule],
})
export class PrimeModules {}