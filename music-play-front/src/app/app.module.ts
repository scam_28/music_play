import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrimeModules } from './_primeng/prime.module';
import { HttpClientModule } from '@angular/common/http';
import { ListMusicComponent } from './components/list-music/list-music.component';
import { SongDetailsComponent } from './components/song-details/song-details.component';
import { FavouritesComponent } from './components/favourites/favourites.component';

@NgModule({
  declarations: [AppComponent, ListMusicComponent, SongDetailsComponent, FavouritesComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PrimeModules,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
